<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="images/favicon.ico" />


    <title>Se connecter</title>

    
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
	
   
	
	<style>
	    body{
		    padding-top:50px;
		    //background-image:url("img/21.jpg");
			background-repeat:no-repeat;
			background-size:cover;
        }
		h1, label{
			color:#0000A0;
		}
	</style>
</head>

  <body>
  
  
  
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <img src="img/21.jpg">
            </div>
            <div class="col-md-8">
                <h1>veuillez  connecter ci-dessous</h1>
                <div class="col-md-8 col-md-offset-1 well">
                    <form method="POST" action="login.php">
                        <div class="form-group">
                            <label for="login">Login : </label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="Entrer login">
                        </div>
                        <div class="form-group">
                            <label for="password">Password : </label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Entrer Password">
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit" name="submit">Se connecter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
	
	


  </body>
</html>
