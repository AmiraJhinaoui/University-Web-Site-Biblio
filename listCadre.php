<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="images/favicon.ico" />


    <title>Liste cadres</title>


    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">

   <?php
   $role = $_SESSION['role'];
       if($role == "admin"){
		   ?>
		   <style>
		       #roleAdmin{
				   display:block;
			   }
		   </style>
		   <?php
	   }else{
		   ?>
		   <style>
		       #roleAdmin{
				   display:none;
			   }
		   </style>
		   <?php
	   }
   ?>
	
	<style>
	    body{
		    padding-top:50px;
		    background-image:url("img/29.jpg");
			background-repeat:no-repeat;
			background-size:cover;
        }
		td{
			color:white;
			font-size:20px;
		}
		th{
			font-size:30px;
			color:white;
		}
		
	</style>
</head>

  <body>




    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="acceuil.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
            </div>
           
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav pull-right">
				    <li id="roleAdmin"><a href="ajoutercadre.php"><span class="glyphicon glyphicon-plus"></span> Ajout nouveau cadre</a></li>
					<li>
                        <a href="#" id="navbarUser" data-toggle="dropdown"><strong> <?php echo $_SESSION['nom']; echo" "; echo $_SESSION['prenom'] ?> </strong><span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Deconnection</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            
        </div>
        
    </nav>



<div class="container">
    <table class="table" style="margin-top:10%;background-color: rgba(0,0,0,0.8);">
        <thead>
            <tr>
                
				 <th>Matricule</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>date_embauche</th>
				 <th>tache</th>
		        
				<th>mail</th>
				<th>Tel</th>
				<th id="roleAdmin">Action</th>
            </tr>
        </thead>
        <tbody>
	
	
	
	<?php 
    $dbc=mysqli_connect('localhost','root','','faculte');
    $sql=mysqli_query($dbc,"select * from cadre");
	while($res=mysqli_fetch_array($sql)){
		$matricule = $res["matricule"];
	    $nom = $res["nom"];
	    $prenom = $res["prenom"];
		
		$date_embauche = $res["date_embauche"];
		$tache=$res["tache"];
		$mail = $res["mail"];
		$tel = $res["tel"];
    ?>
	
	
      <tr>
        <td><?php echo"$matricule" ?></td>
        <td><?php echo"$nom" ?></td>
        <td><?php echo"$prenom" ?></td>
        <td><?php echo"$date_embauche" ?></td>
		<td><?php echo"$tache" ?></td>
		
		<td><?php echo"$mail" ?></td>
		<td><strong><?php echo"$tel" ?></strong></td>
		<td id="roleAdmin">
		    <a class="btn btn-primary" href="modifiercadre.php?id=<?php echo "$matricule";?>&nom=<?php echo "$nom";?>&prenom=<?php echo "$prenom";?>&date_embauche=<?php echo"$date_embauche" ?>&mail=<?php echo "$mail" ?>&tel=<?php echo"$tel" ?>"><span class="glyphicon glyphicon-pencil"></span></a>
			<a class="btn btn-danger" href="supprimercadre.php?id=<?php echo "$matricule";?>" onclick="return(confirm('Ete vous sure de supprimer ?'))"><span class="glyphicon glyphicon-remove"></span></a>
		</td>
      </tr>
	  
	  <?php
	}
?>
        </tbody>
    </table>
	

	
</div>




<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
