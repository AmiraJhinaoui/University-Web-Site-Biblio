<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="images/favicon.ico" />
		
		
		<title>Ajout enseignant</title>
		
		
		<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
		
		<style>
			body{
            padding-top:10%;
            background-image:url("img/25.jpg");
            background-repeat:no-repeat;
            background-size:cover;
			}
			label{
            color:white;
            font-size:20px;
			}
			h2{
            color:#357EC7;
            margin-bottom:30px;
			}
			
		</style>
        
        
	</head>
	
	<body>
		
		
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="acceuil.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						<li>
							<a href="#" id="navbarUser" data-toggle="dropdown"><strong> <?php echo $_SESSION['nom']; echo" "; echo $_SESSION['prenom'] ?> </strong><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Deconnection</a></li>
							</ul>
						</li>
					</ul>
				</div>
				
			</div>
			
		</nav>
		
		
		<div class="container">
			<div class="col-md-6 col-md-offset-2" style="background-color: rgba(0,0,0,0.7);">
				<h2>Ajouter un nouveau etudiant : </h2>
				<form class="form-horizontal" method="POST" action="">
					<div class="form-group">
						<label for="Nom" class="col-sm-4 control-label">Nom : </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="Nom" name="Nom" placeholder="Nom" pattern=".{4,}" title="Entrer un nom de 4 caractére ou plus" required />
							<p class="text-danger"></p>
						</div>
					</div>
					<div class="form-group">
						<label for="Prenom" class="col-sm-4 control-label">Prenom : </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="Prenom" name="Prenom" placeholder="Prenom" pattern=".{4,}" title="Entrer un prenom de 4 caractére ou plus" required />
						</div>
					</div>
					
					
					<div class="form-group">
						<label for="date_naissance" class="col-sm-4 control-label">date_naissance: </label>
						<div class="col-sm-5">
							<input type="date" class="form-control" id="date_naissance" name="date_naissance" placeholder="date_naissance" required />
						</div>
					</div>
					<div class="form-group">
						<label for="date_bac" class="col-sm-4 control-label">date_bac : </label>
						<div class="col-sm-5">
							<input type="date" class="form-control" id="date_bac" name="date_bac" placeholder="date_bac" pattern=".{4,}" title="Entrer un date_bac de 4 caractére ou plus" required />
						</div>
					</div>
					<div class="form-group">
						<label for="mail" class="col-sm-4 control-label">mail : </label>
						<div class="col-sm-5">
							<input type="mail" class="form-control" id="mail" name="mail" placeholder="mail" required />
						</div>
					</div>
					<div class="form-group">
						<label for="Telephone" class="col-sm-4 control-label">Telephone : </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="Telephone" name="Telephone" placeholder="Telephone" pattern="[0-9]{8}" required title="Entrer un numero valide en 8 chiffre" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="ajouter" name="ajouter" class="btn btn-primary">Ajouter</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
		
		
		
		<?php 
			
			if(isset($_POST["ajouter"]))
			{
				$dbc=mysqli_connect('localhost','root','','faculte');
				$nom = $_POST["Nom"];
				$prenom = $_POST["Prenom"];
				$date_naissance = $_POST["date_naissance"];
				$date_bac = $_POST["date_bac"];
				
				$mail = $_POST["mail"];
				$tel = $_POST["Telephone"];
				$sql=mysqli_query($dbc,"insert into etud(nom,prenom,date_naissance,date_bac,mail,tel) values ('$nom','$prenom','$date_naissance','$date_bac','$mail','$tel')");
				if($sql)
				{ 
				?>
				<script type="text/javascript"> alert("etudiant ajouter avec succées");window.location="listEtudiant.php";</script>
				<?php
					} else {
				?>
				<script type="text/javascript"> alert("echec d'ajout");</script>
				<?php
				}
			}
		?>
		
		
		<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
		<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
	</body>
</html>
