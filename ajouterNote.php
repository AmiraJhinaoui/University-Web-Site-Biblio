<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="images/favicon.ico" />
		
		
		<title>Ajout livre</title>
		
		
		<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
		
		<style>
			body{
		    padding-top:10%;
			background-image:url("img/31.jpg");
			background-repeat:no-repeat;
			background-size:cover;
			}
			label{
			color:white;
			font-size:20px;
			}
			h2{
			color:#357EC7;
			margin-bottom:30px;
			}
			
		</style>
		
		
	</head>
	
	<body>
		
		
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="acceuil.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						<li>
							<a href="#" id="navbarUser" data-toggle="dropdown"><strong> <?php echo $_SESSION['nom']; echo" "; echo $_SESSION['prenom'] ?> </strong><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Deconnection</a></li>
							</ul>
						</li>
					</ul>
				</div>
				
			</div>
			
		</nav>
		
		
		<div class="container">
			<div class="col-md-6 col-md-offset-2" style="background-color: rgba(0,0,0,0.7);">
				<h2>Ajouter une nouvelle note: </h2>
				<form class="form-horizontal" method="POST" action="">
					<div class="form-group">
						<label for="num_ce" class="col-sm-4 control-label">Etudiant : </label>
						<div class="col-sm-5">
							<select id="num_ce" name="num_ce" class="form-control">
							<?php 
							$dbc=mysqli_connect('localhost','root','','faculte');
							$sqlEtud=mysqli_query($dbc,"select * from etud");
							while($resEtud=mysqli_fetch_array($sqlEtud)){
								$nce = $resEtud["nce"];
								$nomEtud = $resEtud["nom"];
								$prenomEtud = $resEtud["prenom"];
								
							?>
								<option value="<?php echo $nce ?>"><?php echo $nomEtud ?> <?php echo $prenomEtud ?></option>
							<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="code_module" class="col-sm-4 control-label">code_module : </label>
						
						<div class="col-sm-5">
							<select id="code_module" name="code_module" class="form-control">
							<?php 
							$dbc=mysqli_connect('localhost','root','','faculte');
							$sqlModu=mysqli_query($dbc,"select * from module");
							while($resModu=mysqli_fetch_array($sqlModu)){
								$codeM = $resModu["code_M"];
								$nomM = $resModu["nom_M"];
								
							?>
								<option value="<?php echo $codeM ?>"><?php echo $nomM ?></option>
							<?php } ?>
							</select>
						</div>
					</div>
					
					
					<div class="form-group">
						<label for="note" class="col-sm-4 control-label">note : </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="note" name="note" placeholder="note" required />
						</div>
					</div>
					<!--<div class="form-group">
						<label for="demande_double_corr" class="col-sm-4 control-label">demande_double_corr : </label>
						<div class="col-sm-5">
						<input type="text" class="form-control" id="demande_double_corr" name="demande_double_corr" placeholder="demande_double_corr" pattern=".{1,}" title="Entrer  disponibilité" required />
						</div>
					</div>-->
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="ajouter" name="ajouter" class="btn btn-primary">Ajouter</button>
						</div>
					</div>
					
					
					
					
					
				</form>
				
				
				
				<?php 
					
					if(isset($_POST["ajouter"]))
					{
						$dbc=mysqli_connect('localhost','root','','faculte');
						$num_ce = $_POST["num_ce"];
						$code_module = $_POST["code_module"];
						$note = $_POST["note"];
						
						$sql=mysqli_query($dbc,"insert into note(num_ce,code_module,note) values ($num_ce,$code_module,'$note')");
						if($sql)
						{ 
						?>
						<script type="text/javascript"> alert("note ajoutée avec succées");window.location="listeNote.php";</script>
						<?php
							} else {
						?>
						<script type="text/javascript"> alert("echec d'ajout");</script>
						<?php
						}
					}
				?>
				
				
				<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
				<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
			</body>
		</html>
		