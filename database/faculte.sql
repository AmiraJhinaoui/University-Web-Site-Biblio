-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 26 Mai 2016 à 04:37
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `faculte`
--

-- --------------------------------------------------------

--
-- Structure de la table `biblio`
--

CREATE TABLE IF NOT EXISTS `biblio` (
  `num_l` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(40) NOT NULL,
  `nce` int(8) DEFAULT NULL,
  `disponible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`num_l`),
  KEY `fkn2` (`nce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `biblio`
--

INSERT INTO `biblio` (`num_l`, `titre`, `nce`, `disponible`) VALUES
(1, 'L''insoutenable légèreté de l''etre', 6961250, 0),
(2, 'Desert', 23214786, 1),
(3, 'lk,jnkb', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `cadre`
--

CREATE TABLE IF NOT EXISTS `cadre` (
  `matricule` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `date_embauche` date NOT NULL,
  `tache` varchar(25) NOT NULL,
  `mail` text NOT NULL,
  `tel` int(8) NOT NULL,
  PRIMARY KEY (`matricule`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=99654212 ;

--
-- Contenu de la table `cadre`
--

INSERT INTO `cadre` (`matricule`, `nom`, `prenom`, `date_embauche`, `tache`, `mail`, `tel`) VALUES
(58429687, 'Zarrouk', 'Mohamed', '1990-04-05', 'equipement ', 'zarrouk.med@gmail.com', 99123456),
(58749632, 'Ben Cherifa', 'Ahlem', '1994-02-01', 'Chef de departement', 'ahlem_benchrifa@yahoo.fr', 99853647),
(99654211, 'aaaaty', 'bbbb', '2013-05-07', 'equipement ', 'hgdozmf@gmail.com', 55487236);

-- --------------------------------------------------------

--
-- Structure de la table `emprunte`
--

CREATE TABLE IF NOT EXISTS `emprunte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_l` int(11) NOT NULL,
  `nce` int(8) NOT NULL,
  `date_emp` date NOT NULL,
  `date_rem` date DEFAULT NULL,
  PRIMARY KEY (`id`,`nce`),
  KEY `fk4` (`nce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Structure de la table `ens`
--

CREATE TABLE IF NOT EXISTS `ens` (
  `matricule` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `date_embauche` date NOT NULL,
  `grade` varchar(25) NOT NULL,
  `specialite` varchar(25) NOT NULL,
  `mail` text NOT NULL,
  `tel` int(8) NOT NULL,
  PRIMARY KEY (`matricule`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11875434 ;

--
-- Contenu de la table `ens`
--

INSERT INTO `ens` (`matricule`, `nom`, `prenom`, `date_embauche`, `grade`, `specialite`, `mail`, `tel`) VALUES
(10235478, 'Denden', 'Islem', '2013-10-10', 'Assistant', 'WEB', 'denden_islem.rnu@fst.tn', 24518623),
(11875428, 'Ben Yayha', 'Sadok', '1994-01-01', 'Professeur ', 'WEB', 'sadok.rnu@fst.tn', 0),
(11875429, 'mabrouk', 'asma ', '2010-05-07', 'assistante', 'reseau', 'assma@yahoo.fr', 78215456),
(11875432, 'mabrouk', 'bbbb', '2013-05-07', 'assistante', 'reseau', 'aminamabrouk@gmail.com', 0),
(11875433, 'rrrr', 'ttttt', '2013-05-07', 'assistante', 'ensn', 'marwlejmi@yahoo.fr', 12345678);

-- --------------------------------------------------------

--
-- Structure de la table `etud`
--

CREATE TABLE IF NOT EXISTS `etud` (
  `nce` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `date_naissance` date NOT NULL,
  `date_bac` date NOT NULL,
  `mail` text NOT NULL,
  `tel` int(8) NOT NULL,
  PRIMARY KEY (`nce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23214789 ;

--
-- Contenu de la table `etud`
--

INSERT INTO `etud` (`nce`, `nom`, `prenom`, `date_naissance`, `date_bac`, `mail`, `tel`) VALUES
(6961250, 'Meshry', 'Fatymah', '1994-07-30', '2013-06-30', 'fatymah-meshry@gmail.com', 98581364),
(11856954, 'gazzeh', 'oussema', '1993-11-02', '2012-06-30', 'gazzeh_oussema@gmail.com', 23548967),
(23214786, 'mabrouk', 'bbbb', '1994-11-07', '2013-06-30', 'marwlejmi@yahoo.fr', 0),
(23214787, 'mhamed', 'hattab', '1997-06-13', '2016-05-02', 'mhamed.httb@gmail.com', 0),
(23214788, 'Mechri', 'Fatma', '1991-06-06', '2016-05-18', 'amira@jh.com', 55221447);

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `code_M` int(3) NOT NULL AUTO_INCREMENT,
  `nom_M` varchar(25) NOT NULL,
  `coeff` int(2) NOT NULL,
  PRIMARY KEY (`code_M`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `module`
--

INSERT INTO `module` (`code_M`, `nom_M`, `coeff`) VALUES
(1, 'Mathematiques', 12),
(2, 'Languessss', 3);

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `num_ce` int(8) NOT NULL AUTO_INCREMENT,
  `code_module` int(3) NOT NULL,
  `note` float NOT NULL,
  `demande_double_corr` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`num_ce`,`code_module`),
  KEY `num_ce` (`num_ce`),
  KEY `fkn1` (`code_module`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23214789 ;

--
-- Contenu de la table `note`
--

INSERT INTO `note` (`num_ce`, `code_module`, `note`, `demande_double_corr`) VALUES
(6961250, 1, 8, 0),
(6961250, 2, 12, 0),
(23214788, 1, 13, 1),
(23214788, 2, 18, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(8) NOT NULL,
  `role` varchar(30) NOT NULL,
  `nce_etud` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id_user`, `nom`, `prenom`, `email`, `login`, `password`, `role`, `nce_etud`) VALUES
(1, 'mechri', 'fatma', 'fatimamechri94@gmail', 'fatma', '123456', 'user', 23214788),
(2, 'Jhinaoui', 'Amira', 'amira.jh@yahoo.fr', 'admin', 'admin', 'admin', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_l` int(11) NOT NULL,
  `nce` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `wishlist`
--

INSERT INTO `wishlist` (`id`, `num_l`, `nce`) VALUES
(5, 1, 23214788);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `biblio`
--
ALTER TABLE `biblio`
  ADD CONSTRAINT `fkn2` FOREIGN KEY (`nce`) REFERENCES `etud` (`nce`);

--
-- Contraintes pour la table `emprunte`
--
ALTER TABLE `emprunte`
  ADD CONSTRAINT `fk4` FOREIGN KEY (`nce`) REFERENCES `etud` (`nce`),
  ADD CONSTRAINT `fk5` FOREIGN KEY (`id`) REFERENCES `biblio` (`num_l`);

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `fkn` FOREIGN KEY (`num_ce`) REFERENCES `etud` (`nce`),
  ADD CONSTRAINT `fkn1` FOREIGN KEY (`code_module`) REFERENCES `module` (`code_M`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
