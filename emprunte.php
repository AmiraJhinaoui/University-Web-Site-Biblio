<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="images/favicon.ico" />


    <title>Liste des date_emps</title>


    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">

   <?php
   $role = $_SESSION['role'];
       if($role == "admin"){
		   ?>
		   <style>
		       #roleAdmin{
				   display:block;
			   }
		   </style>
		   <?php
	   }else{
		   ?>
		   <style>
		       #roleAdmin{
				   display:none;
			   }
		   </style>
		   <?php
	   }
   ?>
	
	<style>
	    body{
		    padding-top:50px;
		    background-image:url("img/03.png");
			background-repeat:no-repeat;
			background-size:cover;
        }
		td{
			color:white;
			font-size:20px;
		}
		th{
			font-size:30px;
			color:white;
		}
		
	</style>
</head>

  <body>




    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="acceuil.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
            </div>
           
            
        
    </nav>



<div class="container">
    <table class="table" style="margin-top:10%;background-color: rgba(0,0,0,0.8);">
        <thead>
            <tr>
                
				<th>numl</th>
                <th>nce</th>
                <th>date_emp</th>
				<th>date_rem</th>
				<th id="roleAdmin">Action</th>
            </tr>
        </thead>
        <tbody>
	
	
	
	<?php 
    $dbc=mysqli_connect('localhost','root','','faculte');
    $sql=mysqli_query($dbc,"select * from emprunte");

	while($res=mysqli_fetch_array($sql)){
		$numl = $res["numl"];
	    $nce = $res["nce"];
	    $date_emp = $res["date_emp"];
		$date_rem = $res["date_rem"];
		
    ?>
	
	
      <tr>
        <td><?php echo"$numl" ?></td>
        <td><?php echo"$nce" ?></td>
        <td><?php echo"$date_emp" ?></td>
		<td><?php echo"$date_rem" ?></td>
		
		<td id="roleAdmin">
		    <a class="btn btn-primary" href="rectifierdate_emp.php?id=<?php echo "$numl";?>&nce=<?php echo "$nce";?>&date_emp=<?php echo "$date_emp";?>&date_rem=<?php echo"$date_rem" ?>"><span class="glyphicon glyphicon-pencil"></span></a>
			
		</td>
      </tr>
	  
	  <?php
	}
?>
        </tbody>
    </table>
	

	
</div>




<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
