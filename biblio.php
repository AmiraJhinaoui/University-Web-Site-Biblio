<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="images/favicon.ico" />
		
		
		<title>Liste des nces</title>
		
		
		<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
		
		<?php
			$role = $_SESSION['role'];
			if($role == "admin"){
			?>
			<style>
				#roleAdmin{
				display:block;
				}
			</style>
			<?php
				}else{
			?>
			<style>
				#roleAdmin{
				display:none;
				}
			</style>
			<?php
			}
		?>
		
		<style>
			body{
		    padding-top:50px;
		    background-image:url("img/19.jpg");
			background-repeat:no-repeat;
			background-size:cover;
			}
			td{
			color:white;
			font-size:20px;
			}
			th{
			font-size:30px;
			color:white;
			}
			
		</style>
	</head>
	
	<body>
		
		
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="acceuil.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						<li id="roleAdmin"><a href="ajouterlivre.php"><span class="glyphicon glyphicon-plus"></span> Ajout nouveau livre</a></li>
					</ul></div>
					
			</nav>
			
			
			<br>
			<center>
				<form class="form-inline center" method="GET" action="listEmpruntedParEtud.php">
					
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3">Etudiant</label>
						<select id="nce" name="nce" class="form-control">
							<?php 
								$dbc=mysqli_connect('localhost','root','','faculte');
								$sqlEtud=mysqli_query($dbc,"select * from etud");
								while($resEtud=mysqli_fetch_array($sqlEtud)){
									$nce = $resEtud["nce"];
									$nomEtud = $resEtud["nom"];
									$prenomEtud = $resEtud["prenom"];
									
								?>
								<option value="<?php echo $nce ?>"><?php echo $nomEtud ?> <?php echo $prenomEtud ?></option>
							<?php } ?>
						</select>
					</div>
					
					<button  type="submit" class="btn btn-default" name="searchEmprunt" > liste des livre emprunte par cet etudiant  </button>
				</form>
			</center>
			
			<!--<a class="btn btn-danger" href="empetud.php" onclick=""><span class="glyphicon glyphicon-remove"></span></a>-->
			
			
			<div class="container">
				<table class="table" style="margin-top:10%;background-color: rgba(0,0,0,0.8);">
					<thead>
						<tr>
							
							<th>num_l</th>
							<th>titre</th>
							<!--<th>nce</th>-->
							<th>disponible</th>
							<th id="roleAdmin">Action</th>
						</tr>
					</thead>
					<tbody>
						
						
						
						<?php 
							$dbc=mysqli_connect('localhost','root','','faculte');
							$sql=mysqli_query($dbc,"select * from biblio");
							while($res=mysqli_fetch_array($sql)){
								$num_l = $res["num_l"];
								$titre = $res["titre"];
								$nce = $res["nce"];
								$disponible = $res["disponible"];
								if($disponible == 0){
									$dispo = "Non disponible";
									}else{
									$dispo = "Disponible";
								}
								
							?>
							
							
							<tr>
								<td><?php echo"$num_l" ?></td>
								<td><?php echo"$titre" ?></td>
								<!--<td><?php echo"$nce" ?></td>-->
								<td><?php echo"$dispo" ?></td>
								
								
								<td>
									<a class="btn btn-primary" href="ModifierLivre.php?numL=<?php echo "$num_l";?>"><span class="glyphicon glyphicon-pencil"></span></a>
									<a class="btn btn-danger" href="supprimerLivre.php?numL=<?php echo "$num_l";?>" onclick="return(confirm('Ete vous sure de supprimer ?'))"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
							
							<?php
							}
						?>
					</tbody>
				</table>
				
				
				
			</div>
			
			
			
			
			<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
			<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
		</body>
	</html>
