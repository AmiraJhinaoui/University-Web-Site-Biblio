<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="images/favicon.ico" />
		
		
		<title>Acceuil</title>
		
		
		<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
		
		
		
		
		<style>
			body{
		    padding-top:50px;
		    background-image:url("img/26.jpg");
			background-repeat:no-repeat;
			background-size:cover;
			}
			
		</style>
	</head>
	
	<body>
		
		
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="acceuilUser.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
				</div>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="biblioUser.php" id="ddh"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Bibliotheque</a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						
						<li><a href="listNoteUser.php "><strong>Liste des notes</strong></a></li>
						
						<li>
							<a href="#" id="navbarUser" data-toggle="dropdown"><strong> <?php echo $_SESSION['nom']; echo" "; echo $_SESSION['prenom'] ?> </strong><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Deconnection</a></li>
							</ul>
						</li>
						
					</ul>
				</div>
				
			</div>
			
		</nav>
		
		
		
		<div class="container">
			<div class="col-md-6 well" style="margin-top:35%">
				<h4><b><u>Faculté des sciences de Tunis </u></b></h4>
				<p class="text-primary">La Faculté des sciences de Tunis (كلية العلوم بتونس) ou FST, de son nom complet Faculté des sciences mathématiques,
					physiques et naturelles de Tunis,
					est une faculté située dans le campus d'El Manar à Tunis (Tunisie).
				Elle dépend de l'Université de Tunis - El Manar. </p>
			</div>
		</div>
		
		
		
		
		<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
		<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
	</body>
</html>
