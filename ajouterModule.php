<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="images/favicon.ico" />
		
		
		<title>Ajout cadre administratif</title>
		
		
		<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
		
		<style>
			body{
		    padding-top:10%;
			background-image:url("img/6.jpg");
			background-repeat:no-repeat;
			background-size:cover;
			}
			label{
			color:white;
			font-size:20px;
			}
			h2{
			color:#357EC7;
			margin-bottom:30px;
			}
			
		</style>
		
		
	</head>
	
	<body>
		
		
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="acceuil.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						<li>
							<a href="#" id="navbarUser" data-toggle="dropdown"><strong> <?php echo $_SESSION['nom']; echo" "; echo $_SESSION['prenom'] ?> </strong><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Deconnection</a></li>
							</ul>
						</li>
					</ul>
				</div>
				
			</div>
			
		</nav>
		
		
		<div class="container">
			<div class="col-md-6 col-md-offset-2" style="background-color: rgba(0,0,0,0.7);">
				<h2>Ajouter un nouveau module: </h2>
				<form class="form-horizontal" method="POST" action="">
					<div class="form-group">
						<label for="Nom" class="col-sm-4 control-label">Nom : </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="Nom" name="Nom" placeholder="Nom module" pattern=".{4,}" title="Entrer un nom de 4 caractére ou plus" required />
							<p class="text-danger"></p>
						</div>
					</div>
					<div class="form-group">
						<label for="Prenom" class="col-sm-4 control-label">Coefficient : </label>
						<div class="col-sm-5">
							<input type="number" class="form-control" id="coeff" name="coeff" placeholder="Coefficient"  required />
						</div>
					</div>
					
					
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="ajouter" name="ajouter" class="btn btn-primary">Ajouter</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		
		
		<?php 
			
			if(isset($_POST["ajouter"]))
			{
				$dbc=mysqli_connect('localhost','root','','faculte');
				$nom = $_POST["Nom"];
				$coeff = $_POST["coeff"];
				
				$sql=mysqli_query($dbc,"insert into module(nom_M,coeff) values ('$nom','$coeff')");
				if($sql)
				{ 
				?>
				<script type="text/javascript"> alert("Module ajouter avec succée");window.location="listModules.php";</script>
				<?php
					} else {
				?>
				<script type="text/javascript"> alert("echec d'ajout");</script>
				<?php
				}
			}
		?>
		
		
		<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
		<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
	</body>
</html>
