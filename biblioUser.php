<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
		<meta name="author" content="GeeksLabs">
		<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
		<link rel="shortcut icon" href="images/favicon.ico" />
		
		
		<title>Liste des nces</title>
		
		
		<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
		
		
		
		<style>
			body{
		    padding-top:50px;
		    background-image:url("img/19.jpg");
			background-repeat:no-repeat;
			background-size:cover;
			}
			td{
			color:white;
			font-size:20px;
			}
			th{
			font-size:30px;
			color:white;
			}
			
		</style>
	</head>
	
	<body>
		
		
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="acceuilUser.php" id="ddr"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Acceuil</a>
				</div>
				
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						
						
						<li>
							<a href="#" id="navbarUser" data-toggle="dropdown"><strong>Mes Livres </strong><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="listEmprunteUser.php">Livres Empruntées</a></li>
								<li><a href="wishlistUser.php"> Mes Wishlist</a></li>
							</ul>
						</li>
						
					</ul>
				</div>
				
				
			</nav>
			
			
			
			
			
			
			
			<div class="container">
				<table class="table" style="margin-top:10%;background-color: rgba(0,0,0,0.8);">
					<thead>
						<tr>
							
							<th>num_l</th>
							<th>titre</th>
							<!--<th>nce</th>-->
							<th>disponible</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						
						
						
						<?php 
							$dbc=mysqli_connect('localhost','root','','faculte');
							$sql=mysqli_query($dbc,"select * from biblio");
							while($res=mysqli_fetch_array($sql)){
								$num_l = $res["num_l"];
								$titre = $res["titre"];
								$nce = $res["nce"];
								$disponible = $res["disponible"];
								if($disponible == 0){
									$dispo = "Non disponible";
									}else{
									$dispo = "Disponible";
								}
								
								
								
							?>
							
							
							<tr>
								<td><?php echo"$num_l" ?></td>
								<td><?php echo"$titre" ?></td>
								<!--<td><?php echo"$nce" ?></td>-->
								<td><?php echo"$dispo" ?></td>
								
								<td>
									<?php if($disponible == 0){
										
										
									?>
									<a title="Mettre livre dans WishList" class="btn btn-primary" href="addWishlist.php?nceEtud=<?php echo $_SESSION['nom'] ?>&numL=<?php echo"$num_l" ?>"><span class="glyphicon glyphicon-heart"></span></a>
									<?php
										} else{
										
									?>
									<a title="Emprunter livre" class="btn btn-info" href="emprunteUser.php?nceEtud=<?php echo $_SESSION['nom'] ?>&numL=<?php echo"$num_l" ?>" onclick="return confirm('sure ? ')"><span class="glyphicon glyphicon-book"></span></a>
									<?php
									}
									?>
									
									
									
									
								</td>
							</tr>
							
							<?php
							}
						?>
					</tbody>
				</table>
				
				
				
			</div>
			
			
			
			
			<script type="text/javascript" src="Bootstrap/jquery-1.11.2.min.js"></script>
			<script src="Bootstrap/dist/js/bootstrap.min.js"></script>
		</body>
	</html>
